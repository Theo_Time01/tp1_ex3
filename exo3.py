#!/usr/bin/env python
# coding: utf-8
"""
author : theotime Perrichet

-Ce code définit une classe qui fait des opérations arithmétiques avec des complexes
"""


class SimpleComplexCalculator:
    """simple classe model, SimpleCalculator class"""

    def __init__(self, complex_1, complex_2):
        """constructeur : on associe deux variables correspondant
        aux deux tuples qui initialisent l'objet de la classe"""
        self.complex_1 = complex_1
        self.complex_2 = complex_2

    # Fonction somme
    def sum_complex(self):
        """On récupère les deux attributs de l'objet et on
        effectue la somme complexe"""
        return [
            self.complex_1[0] + self.complex_2[0],
            self.complex_1[1] + self.complex_2[1],
        ]

    # Fonction soustraction
    def substract_complex(self):
        """On récupère les deux attributs de l'objet et on
        effectue la soustraction complexe"""
        return [
            self.complex_1[0] - self.complex_2[0],
            self.complex_1[1] - self.complex_2[1],
        ]

    # Fonction multiplier
    def multiply_complex(self):
        """On récupère les deux attributs de l'objet et on
        effectue la multiplication complexe"""
        return [
            self.complex_1[0] * self.complex_2[0]
            - self.complex_1[1] * self.complex_2[1],
            self.complex_1[0] * self.complex_2[1]
            + self.complex_1[1] * self.complex_2[0],
        ]

    # Fonction diviser
    def divide_complex(self):
        """On récupère les deux attributs de l'objet et
        on effectue la division complexe"""
        if (
                self.complex_1[0] != 0
                and self.complex_2[0] != 0
                and self.complex_1[1] != 0
                and self.complex_2[1] != 0
        ):  # verification qu'aucun des attributs n'est nul
            real = (
                self.complex_1[0] * self.complex_2[0]
                + self.complex_1[1] * self.complex_2[1]
            ) / (self.complex_2[0] ** 2 + self.complex_2[1] ** 2)
            imag = (
                self.complex_1[1] * self.complex_2[0]
                - self.complex_1[0] * self.complex_2[1]
            ) / (self.complex_2[0] ** 2 + self.complex_2[1] ** 2)
            return [real, imag]


#Script main qui utilise les deux instances de la
#classe SimpleComplexCalculator pour faire des tests.
if __name__ == "__main__":
    TEST = SimpleComplexCalculator(
        [1, 2], [1, 3]
    )  # crée une instance de SimpleComplexCalculator
    print(
        "somme : ", TEST.sum_complex()
    )  # affiche la chaine de caractères "somme : " et le résultat de la méthode sum_complex
    print(
        "soustraction : ", TEST.substract_complex()
    )  # affiche le résultat de la méthode substract_complex
    print(
        "multiplication : ", TEST.multiply_complex()
    )  # affiche le résultat de la méthode multiply_complex
    print(
        "division : ", TEST.divide_complex()
    )  # affiche le résultat de la méthode divide_complex
